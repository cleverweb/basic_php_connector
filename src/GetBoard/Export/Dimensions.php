<?php

namespace GetBoard\Export;

use Perfico\Model\DataSource;
use Perfico\Model\DataSourceDimension;
use Perfico\Model\SourceValue;
use Perfico\Exporter\BoardioExporter;

class Dimensions implements BoardioExporter
{

    public function after()
    {
        // TODO: Implement after() method.
    }

    public function before()
    {
        // TODO: Implement before() method.
    }

    /**
     * @return DataSource | DataSource[]
     */
    public function run()
    {
        return $this->salesByManagerPerMonth();
    }

    private function salesByManagerPerMonth() {
        $dataSource = new DataSource("sales-by-manager-per-month");
        $dataSource->name = "Sales by manager per month";
        $dataSource->addSourceDimension(new DataSourceDimension("manager", "Manager"));

        $data = [
            "John Doe" => [20000, 23000],
            "Jane Doe" => [9000, 13000],
            "Jens Hansen" => [9900, 16000],
            "Joe Farnarkle" => [13000, 35000]
        ];

        for($i = 12; $i > 0; $i--) {

            foreach($data as $name => $range) {
                $sourceValue = new SourceValue();
                $sourceValue->current = rand($range[0], $range[1]);
                $sourceValue->name = $i;
                $sourceValue->prefix = "$";
                $sourceValue->addDimension("manager", $name);

                $dataSource->addSourceValue($sourceValue);
            }
        }

        return $dataSource;
    }

}