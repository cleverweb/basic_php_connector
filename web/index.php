<?php

require "../vendor/autoload.php";
use Perfico\HttpRequest\WebHookHandler;
use GetBoard\Export\Dimensions;
use GetBoard\Config;

try {
    /**
     * @var \Perfico\Connection\Response[] $responses
     */
    $responses = WebHookHandler::process(new Dimensions(), new Config());

    foreach($responses as $key => $response) {
        echo $key." = ".($response->isSuccessful()?"OK":"Fail")."<br>";
    }

} catch (\Perfico\Connection\CurlException $e) {
    echo $e->getMessage();
}
