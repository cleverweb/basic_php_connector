#!/usr/bin/env php
<?php

require __DIR__.'/vendor/autoload.php';

use Perfico\Command\LoadExports;
use Perfico\Command\LoadSingleClass;
use Symfony\Component\Console\Application;
use GetBoard\Config;

$application = new Application();

$exportCommand = new LoadExports();
$exportCommand->setConfig(new Config());

$singleClassCommand = new LoadSingleClass();
$singleClassCommand->setConfig(new Config());


$application->add($exportCommand);
$application->add($singleClassCommand);
$application->run();